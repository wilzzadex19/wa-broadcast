<?php $__env->startSection('css'); ?>
    <link href="<?php echo e(rsc('css/select2.min.css')); ?>" rel="stylesheet" />

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">
        Kirim Pesan
        <span style="font-size:50%">
            <nav aria-label="breadcrumb" style="float:right; margin-left:10px;">
                <ol class="breadcrumb" style="margin-bottom: -1rem">
                    <li class="breadcrumb-item">Manual</li>
                </ol>
            </nav>
        </span>
    </h1>
</div>

<!-- Content Row -->
<div class="row">
    <div class="col-md-12">
        <div class="card shadow mb-4">
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary">Form Input Data</h6>
            </div>
            <div class="card-body">
                <form action="<?php echo e(url('messaging/manual')); ?>" method="post" class="row">
                    <?php echo csrf_field(); ?>
                    <div class="col-md-12">
                        <?php if($message = Session::get('nomorKosong')): ?>
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <?php echo e($message); ?>

                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <?php endif; ?>

                        <?php if($message = Session::get('pesanKosong')): ?>
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <?php echo e($message); ?>

                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <?php endif; ?>
                        <?php if($message = Session::get('sukses')): ?>
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <?php echo $message; ?>

                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <?php endif; ?>
                    </div>
                    <div class="col-md-6">

                        <div class="form-group">
                            <label for="nomor_telepon">Nomor Whatsapp</label>
                            <select class="form-control" multiple name="nomor[]" id="nomor_telepon">
                            <?php if($message = Session::get('nomor')): ?>
                                <?php $__currentLoopData = $message; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($value); ?>" selected><?php echo e($value); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php endif; ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nomor_telepon">Isi Pesan</label>
                            <textarea name="pesan" id="" class="form-control" style="width: 100%" rows="10"><?php if($message = Session::get('pesan')): ?><?php echo e($message); ?><?php endif; ?></textarea>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-primary btn-block">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
<script src="<?php echo e(rsc('js/select2.min.js')); ?>"></script>
<script>
    $("#nomor_telepon").select2({
        tags: true,
    });

</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH G:\web\xampp7\htdocs\whatsapp\resources\views/messaging/manual.blade.php ENDPATH**/ ?>