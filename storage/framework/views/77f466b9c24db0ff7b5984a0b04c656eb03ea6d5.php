<?php $__env->startSection('css'); ?>
    <link  href="<?php echo e(rsc('vendor/datatables/dataTables.bootstrap4.min.css')); ?>" rel="stylesheet">


<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">
        Phonebook
        <span style="font-size:50%">
            <nav aria-label="breadcrumb" style="float:right; margin-left:10px;">
                <ol class="breadcrumb" style="margin-bottom: -1rem">
                    <li class="breadcrumb-item"><a href="<?php echo e(url('phonebook')); ?>">Index</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Tambah</li>
                </ol>
            </nav>
        </span>
    </h1>
</div>

<!-- Content Row -->
<div class="row">
    <div class="col-md-8">
        <div class="card shadow mb-4">
            <!-- Card Header - Dropdown -->
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary">Form Input Data</h6>

            </div>
            <!-- Card Body -->
            <div class="card-body">
                <div class="row data-input">
                    <div class="col-md-12">
                        <?php if($message = Session::get('error')): ?>
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <?php echo e($message); ?>

                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <?php endif; ?>
                    </div>
                    <div class="form-group col-md-5">
                        <!-- <label for="nama">Nama</label> -->
                        <input type="text" class="form-control" id="nama" placeholder="Input Nama">
                        <small id="namaHelp" class="form-text text-danger helper" style="display: none;"></small>
                    </div>
                    <div class="form-group col-md-5">
                        <!-- <label for="nomor">Nomor</label> -->
                        <input type="number" class="form-control" id="nomor" placeholder="Input nomor" maxlength="14" min="0">
                        <small id="nomorHelp" class="form-text text-danger helper" style="display: none;"></small>

                    </div>
                    <div class="form-group col-md-2">
                        <a href="javascript:void(0)" class="btn btn-primary tbh-data loading"><i class="fa fa-plus"></i></a>
                        <div class="sk-chase loading" style="display: none">
                            <div class="sk-chase-dot"></div>
                            <div class="sk-chase-dot"></div>
                            <div class="sk-chase-dot"></div>
                            <div class="sk-chase-dot"></div>
                            <div class="sk-chase-dot"></div>
                            <div class="sk-chase-dot"></div>
                        </div>
                    </div>
                </div>
                <form action="<?php echo e(url('phonebook/create')); ?>" method="post" class="">
                    <?php echo csrf_field(); ?>
                    <table id="table" class="table table-striped" style="width:100%">
                        <thead>
                            <tr>
                                <th>Nama</th>
                                <th>Nomor Telepon</th>
                                <th width="10%">Aksi</th>
                            </tr>
                        </thead>
                        <tbody class="dataNomor">

                        </tbody>
                    </table>
                    <hr>
                    <button type="submit" class="btn btn-primary btn-sm float-right" id="submitBtn" disabled><i class="fas fa-save"></i> Simpan </button>
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card shadow mb-4">
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary">Form Upload Data Excel</h6>
            </div>
            <div class="card-body">
                <form action="<?php echo e(url('phonebook/saveExcel')); ?>" method="post" class="row" enctype="multipart/form-data">
                    <?php echo csrf_field(); ?>
                    <div class="col-md-10">
                        <div class="custom-file">
                            <input type="file" name="file_excel" class="custom-file-input" id="customFile">
                            <label class="custom-file-label" for="customFile">Choose file</label>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <a href="<?php echo e(url('public/template/phonebook_template.xlsx')); ?>" class="btn btn-primary" title="download template"><i class="fa fa-download"></i></a>
                    </div>
                    <div class="col-md-12">
                        <button class="btn btn-primary btn-block mt-4 btn-excel" disabled type="submit"><i class="fa fa-save"></i> Simpan </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<section class="hidden" style="display: none;">
    <table>
        <tr class="rowData rows">
            <td>
                <input type="text" name="nama[]" class="nama" style="display:none">
                <span class="namaText">Test</span>
            </td>
            <td>
                <input type="text" name="nomor[]" class="nomor" style="display:none">
                <span class="nomorText">0123</span>
            </td>
            <td>
                <a href="javascript:void(0)" class="hapus btn btn-sm btn-danger"><i class="fa fa-times"></i></a>
            </td>
        </tr>
    </table>
</section>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
<script>
    var no_tlp = [];

    $(document).on('click','.hapus', function () {
        var element = $(this).closest('.rows');
        var nomorTlp = element.find('.nomor').val();
        element.remove();
        no_tlp = no_tlp.filter(function(e) { return e !== nomorTlp });

        if (no_tlp.length === 0) {
            $('#submitBtn').prop('disabled', true);
        }

    });

    $(document).on('change', '#customFile', function () {
        var fileName = $(this).val();
        //replace the "Choose a file" label
        $(this).next('.custom-file-label').html(fileName);
        if( document.getElementById("customFile").files.length == 0 ){
            $('.btn-excel').prop('disabled', true);
        } else {
            $('.btn-excel').prop('disabled', false);
        }
    })

    $(document).on('click','.tbh-data', function () {
        var parent = $(this).closest('.data-input');
        var nomor = parent.find('#nomor').val();
        var nama = parent.find('#nama').val();
        nomor.match(/(\d+)/);

        if (nomor < 0) {
            $('#nomorHelp').text('Mohon input hanya angka').show();

        } else if ((nomor.length > 15) || (nomor.length < 9) || (nama.length < 3)) {
            ((nomor.length > 15) || (nomor.length < 9)) ? $('#nomorHelp').text('Nomor telepon yang diterima 9 - 15 digit').show() : $('#nomorHelp').hide();
            nama.length < 3 ? $('#namaHelp').text('Mohon input nama minimal 3 huruf.').show() : $('#namaHelp').hide();

        // } else if ((nomor.substr(0,2) !== '08') && (nomor.substr(0,2) !== '62')) {

        //     $('#nomorHelp').text('Mohon awali dengan 08 atau 62').show();

        } else if ( no_tlp.length !== 0 && no_tlp.includes(nomor)) {

            $('#nomorHelp').text('Nomor telepon sudah ada.').show();

        } else {

            $.ajax({
                url : "<?php echo e(url('phonebook/ajaxCekNomor')); ?>",
                method : 'POST',
                dataType : 'JSON',
                data : {
                    'nomor' : nomor,
                    'nama' : nama,
                    "_token": "<?php echo e(csrf_token()); ?>",
                },
                beforeSend : function () {
                    $('.helper').hide();
                    $('.loading').toggle();
                },
                success : function (response) {

                    if (response.isExist == true) {
                        $('#nomorHelp').text('Nomor diatas sudah terdaftar atas nama ' + (JSON.parse(response.data)).nama ).show();
                    } else {
                        var row = $('.rowData').clone();
                        row.removeClass('rowData');
                        row.find('.namaText').text(nama);
                        row.find('.nomorText').text(nomor);
                        row.find('.nama').val(nama);
                        row.find('.nomor').val(nomor);
                        row.appendTo(".dataNomor");

                        no_tlp.push(nomor);

                        $('#nama').val('');
                        $('#nomor').val('');
                        $('#submitBtn').prop('disabled', false);
                    }

                    $('.loading').toggle();

                }
            })
        }
    })
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH G:\web\xampp7\htdocs\whatsapp\resources\views/phonebook/add.blade.php ENDPATH**/ ?>