<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Phonebook extends Model
{
    //
    protected $table='phonebook';

    use SoftDeletes;

    public function perusahaan()
    {
        return $this->belongsTo('App\Perusahaan', 'id_perusahaan', 'id');
    }
}
