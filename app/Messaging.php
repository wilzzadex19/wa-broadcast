<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Messaging extends Model
{
    protected $table='messaging';
    //

    public function perusahaan()
    {
        return $this->belongsTo('App\Perusahaan', 'id_perusahaan', 'id');
    }
}
