<?php

namespace App\Imports;

use App\Messaging;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
// use Maatwebsite\Excel\Concerns\ToModel;
// use Maatwebsite\Excel\Concerns\WithMappedCells;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use DB;

class MessageImport implements WithHeadingRow, ToCollection
{
    //
    public function  __construct($pesan)
    {
        $this->pesan = $pesan;
    }
    public function collection(Collection $rows)
    {
        $data = [];
        $idPerusahaan = \Auth::user()->perusahaan->id;
        foreach ($rows as $row) {
            $nomorTujuan = preg_replace('/[^0-9]/', '', $row);
            $aw = substr($nomorTujuan, 0 ,2);
            $no_tujuan = $nomorTujuan;
            if($aw == "08"){
                $no_tujuan = substr_replace($nomorTujuan, '62', 0, 1);
            }else if($aw == "+6"){
                $no_tujuan = substr_replace($nomorTujuan, '62', 0, 3);
            }elseif (substr($aw, 0 ,1) == 8) {
                $no_tujuan = '62'.$nomorTujuan;
            }
            array_push($data, [
                'id_perusahaan' => $idPerusahaan,
                'msisdn' => $no_tujuan,
                'pesan' => $this->pesan,
                'status' => 'on_queue',
                'req' => 'app',
                'created_at'=>date('Y-m-d H:i:s')
            ]);
        }
        $data = Messaging::insert($data);
        return $data;
    }
}
