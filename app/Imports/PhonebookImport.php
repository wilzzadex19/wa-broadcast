<?php

namespace App\Imports;

use App\Phonebook;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
// use Maatwebsite\Excel\Concerns\ToModel;
// use Maatwebsite\Excel\Concerns\WithMappedCells;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use DB;

class PhonebookImport implements WithHeadingRow, ToCollection
{
    //
    public function collection(Collection $rows)
    {
        $data = [];
        $idPerusahaan = \Auth::user()->perusahaan->id;
        foreach ($rows as $row) {
            array_push($data,[
                'nama' => $row['nama'],
                'nomor' => preg_replace('/[^0-9]/', '', $row['nomor_telepon']),
                'id_perusahaan' => $idPerusahaan,
                'unique_id' => $idPerusahaan .'-'.$row['nomor_telepon'],
                'created_at'=>date('Y-m-d H:i:s')
            ]);
        }

        $data = Phonebook::insert($data);
        DB::statement('DELETE FROM phonebook a USING phonebook b WHERE a.id < b.id AND a.unique_id = b.unique_id');
        // dd($data);
        return $data;
    }
}
