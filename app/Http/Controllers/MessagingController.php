<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Imports\MessageImport;
use App\Messaging;
use Illuminate\Http\Request;
use Excel;

class MessagingController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function manual(Request $r)
    {
        return view('messaging.manual');
    }

    public function phone(Request $r)
    {
        return view('messaging.phonebook');
    }

    public function xls(Request $r)
    {
        return view('messaging.xls');
    }

    public function ajaxPesan(Request $r)
    {
        $idPerusahaan = \Auth::user()->perusahaan->id;
        $data = Messaging::where('id_perusahaan', $idPerusahaan)->whereBetween('created_at',[$r->awal, $r->akhir])->get();
        $response = [];
        array_push($response,[
            'total' => $data->count(),
            'terkirim' => $data->where('status','sent')->count(),
            'gagal' => $data->where('status','fail')->count()
        ]);
        return json_encode($response);
    }

    public function saveXls(Request $r)
    {
        if ($r->file('file_excel')->getClientOriginalExtension() !== 'xlsx' || empty($r->pesan)) {
            $pesan = [];
            empty($r->nomor) ? $pesan['error'] = 'Mohon upload file sesuai dengan template' : '';
            empty($r->pesan) ? $pesan['pesanKosong'] = 'Mohon kolom pesan tidak dikosongkan' : '';

            return back()->with($pesan);
        }

        Excel::import(new MessageImport(nl2br($r->pesan)), $r->file('file_excel'));
        return redirect('messaging/xls')->with('sukses','Data berhasil di proses');
    }

    public function saveManual(Request $r)
    {

        $cek = $this->checkIsEmpty($r);
        if ($cek['redirect']) {
            return redirect('messaging/manual')->with($cek);
        }

        $data = $this->arrayDataMessage($r);

        Messaging::insert($data['data']);

        $msg = 'Data berhasil diproses. ';
        $msg .= empty($data['notSukses']) ? '' : '<br/> Selain : '.$data['notSukses'];

        return redirect('messaging/manual')->with('sukses', $msg);
    }

    public function savePhone(Request $r)
    {
        $cek = $this->checkIsEmpty($r);
        if (($cek['redirect'])) {
            return redirect('messaging/phonebook')->with($cek);
        }
        $data = $this->arrayDataMessage($r);
        Messaging::insert($data['data']);
        $msg = 'Data berhasil diproses. ';
        $msg .= empty($data['notSukses']) ? '' : '<br/> Selain : '.$data['notSukses'];

        return redirect('messaging/phonebook')->with('sukses', $msg);
    }

    function checkIsEmpty($r)
    {
        $data = [];
        $data['redirect'] = false;
        if (empty($r->nomor) || empty($r->pesan)) {
            empty($r->nomor) ? $data['nomorKosong'] = 'Mohon kolom nomor tujuan tidak dikosongkan' : $data['nomor'] = $r->nomor;
            empty($r->pesan) ? $data['pesanKosong'] = 'Mohon kolom pesan tidak dikosongkan' : $data['pesan'] = nl2br($r->pesan);
            $data['redirect'] = true;
        }

        return $data;
    }

    function arrayDataMessage($r)
    {
        $idPerusahaan = \Auth::user()->perusahaan->id;
        $notSukses = '';
        $data = [];
        foreach ($r->nomor as $key => $value) {
            $nomorTujuan = preg_replace('/[^0-9]/', '', $value);
            if (empty($nomorTujuan)) {
                $notSukses .= empty($notSukses) ? $value : ', '.$value;
            } else {
                $aw = substr($nomorTujuan, 0 ,2);
                $no_tujuan = $nomorTujuan;
                if($aw == "08"){
                    $no_tujuan = substr_replace($nomorTujuan, '62', 0, 1);
                }else if($aw == "+6"){
                    $no_tujuan = substr_replace($nomorTujuan, '62', 0, 3);
                }elseif (substr($aw, 0 ,1) == 8) {
                    $no_tujuan = '62'.$nomorTujuan;
                }
                array_push($data, [
                    'id_perusahaan' => $idPerusahaan,
                    'msisdn' => $no_tujuan,
                    'pesan' => nl2br($r->pesan),
                    'status' => 'on_queue',
                    'req' => 'app',
                    'created_at'=>date('Y-m-d H:i:s')
                ]);
            }
        }
        $res['data'] = $data;
        $res['notSukses'] = $notSukses;
        return $res;
    }
}
