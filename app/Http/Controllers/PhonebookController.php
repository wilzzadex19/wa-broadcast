<?php

namespace App\Http\Controllers;

use App\Imports\PhonebookImport;
use App\KodeNegara;
use App\Phonebook;
use Illuminate\Http\Request;
use Datatables ;
use Illuminate\Support\Facades\DB;
use Excel;

class PhonebookController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $r)
    {
        return view('phonebook.index');
    }

    public function add(Request $r)
    {
        $kode_negara = KodeNegara::get();
        return view('phonebook.add', compact('kode_negara'));
    }

    public function update(Request $r)
    {
        $data = Phonebook::find($r->id);
        $data->nama = $r->nama;
        $data->nomor = $r->nomor;
        $data->save();

        return redirect('phonebook')->with('status', 'Data Phonebook berhasil diupdate!');
    }

    public function save(Request $r)
    {
        $data = [];
        $idPerusahaan = \Auth::user()->perusahaan->id;
        foreach ($r->nomor as $key => $value) {
            array_push($data,[
                'nomor' => $r->nomor[$key],
                'nama' => $r->nama[$key],
                'id_perusahaan' => $idPerusahaan,
                'unique_id' => $idPerusahaan .'-'.$r->nomor[$key],
                'created_at'=>date('Y-m-d H:i:s')
            ]);
        };

        // dd('test');
        Phonebook::insert($data);
        DB::statement('DELETE FROM phonebook a USING phonebook b WHERE a.id < b.id AND a.unique_id = b.unique_id');

        return redirect('phonebook')->with('status', 'Data Phonebook berhasil diupdate!');

    }

    public function saveExcel(Request $r)
    {
        if ($r->file('file_excel')->getClientOriginalExtension() !== 'xlsx') {
            return back()->with('error', 'Mohon upload file sesuai dengan template');
        }

        $data = Excel::import(new PhonebookImport, $r->file('file_excel'));
        return redirect('phonebook')->with('status', 'Data Phonebook berhasil diupdate!');
    }

    public function delete(Request $r)
    {
        Phonebook::where('id', $r->id)->delete();
        return redirect('phonebook')->with('status', 'Data Phonebook berhasil dihapus!');
    }

    public function ajaxCekNomor(Request $r)
    {
        $idPerusahaan = \Auth::user()->perusahaan->id;
        $nomor = preg_replace('/[^0-9]/', '', $r->nomor);

        $kondisi = ['nomor' => $nomor, 'id_perusahaan'=> $idPerusahaan];

        $data = Phonebook::where($kondisi)->first();
        $cekdata = empty($data) ? false : true ;

        if ($cekdata) {
            $response = collect(['isExist' => true, 'data' => $data->toJson() ]) ;
        } else {
            $response = collect(['isExist' => false])->toJson();
        }
        return $response;
    }

    public function ajaxIndex(Request $r)
    {
        $query = Phonebook::select('id','nomor', 'nama')->where('id_perusahaan', \Auth::user()->perusahaan->id);

        return Datatables::eloquent($query)
        ->addColumn('aksi', function($data) {
            $btn =  '<a href="javascript:void(0)" class="btn shadow-sm btn-sm btn-warning edit" data-nomor="'.$data->nomor.'" data-nama="'.$data->nama.'" data-id="'. $data->id .'" data-toggle="modal" data-target="#editModal"><i class="fas fa-sm fa-magic"></i></a>';
            $btn .= '&nbsp;' ;
            $btn .= '<a href="javascript:void(0)" class="btn shadow-sm btn-sm btn-danger delete" data-toggle="modal" data-target="#deleteModal" data-url="'. url('phonebook/delete', $data->id) .'"><i class="fas fa-sm fa-times"></i></a>';
            return $btn;
        })
        ->rawColumns(['aksi'])
        ->make(true);
    }

    public function ajaxSelect(Request $r)
    {
        // dd($r->all());
        $data = Phonebook::select('nomor','nama')->where('nomor','ILIKE','%'.$r->q.'%')->orWhere('nama','ILIKE','%'.$r->q.'%')->get();

        $response = [];

        foreach ($data as $key => $value) {
            array_push($response,[
                'id' => $value->nomor,
                'text' => $value->nama . " (".$value->nomor.")"
            ]);
        }
        return json_encode($response);
    }

}
