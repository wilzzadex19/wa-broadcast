<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Messaging;
use Illuminate\Http\Request;
use Datatables;

class LogController extends Controller
{
    //

    public function index()
    {
        return view('log.index');
    }

    public function ajaxIndex(Request $r)
    {

        $query = Messaging::where('id_perusahaan', \Auth::user()->perusahaan->id)->orderByDesc('id');

        return Datatables::eloquent($query)
        ->addColumn('dateTime',function ($item){
            return date('d F Y', strtotime($item->created_at)) . ' ' . date('H:i:s', strtotime($item->created_at));
        })
        ->rawColumns(['pesan', 'dateTime'])
        ->make(true);
    }

    public function antrian()
    {
        $data = Messaging::where('status','on_queue')->orderByDesc('created_at')->get();

        return view('log/queue',compact('data'));
    }
}
