<?php

if (! function_exists('rsc')) {
    /**
     * Generate an rsc path for the application.
     *
     * @param  string  $path
     * @param  bool    $secure
     * @return string
     */
    function rsc($path, $secure = null)
    {
        return app('url')->asset("public/assets/".$path, $secure);
    }
}

if (! function_exists('activePath')) {
    function activePath($name)
    {
        $res = request()->is($name.'*') ? 'active' : '';
        return $res;
    }
}