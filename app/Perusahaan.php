<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Perusahaan extends Model
{
    protected $table='perusahaan';

    use SoftDeletes;
    //

    public function users()
    {
        return $this->hasMany('App\User', 'id_perusahaan', 'id');
    }

    public function FunctionName(Type $var = null)
    {
        return $this->hasMany('App\Messaging', 'id_perusahaan', 'id');
    }
}
