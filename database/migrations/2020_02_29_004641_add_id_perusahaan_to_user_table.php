<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIdPerusahaanToUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->bigInteger('id_perusahaan')->after('id')->nullable();
            $table->softDeletes();
        });

        Schema::table('phonebook', function (Blueprint $table) {
            //
            $table->softDeletes();
        });

        Schema::table('perusahaan', function (Blueprint $table) {
            //
            $table->softDeletes();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
            $table->dropColumn('id_perusahaan');
        });
    }
}
