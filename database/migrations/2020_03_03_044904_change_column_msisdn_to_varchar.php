<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeColumnMsisdnToVarchar extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('messaging', function (Blueprint $table) {
            //
            $table->dropColumn('msisdn');
        });
        Schema::table('messaging', function (Blueprint $table) {
            //
            $table->string('msisdn');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('messaging', function (Blueprint $table) {
            //
            $table->dropColumn('msisdn');
        });
        Schema::table('messaging', function (Blueprint $table) {
            //
            $table->bigInteger('msisdn');
        });
    }
}
