<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNullableToKeteranganColumnMessagingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('messaging', function (Blueprint $table) {
            //
            $table->dropColumn('keterangan');
        });
        Schema::table('messaging', function (Blueprint $table) {
            //
            $table->string('keterangan')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('messaging', function (Blueprint $table) {
            //
            $table->dropColumn('keterangan');
        });
        Schema::table('messaging', function (Blueprint $table) {
            //
            $table->string('keterangan');
        });
    }
}
