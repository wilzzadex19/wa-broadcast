// declare initial variables
var app = require('express')();
var port = 3000
var http = require('http').createServer(app).listen(port, "0.0.0.0");
var io = require('socket.io')(http);
var request = require("request");
var dateFormat = require('dateformat');

// declare connections
const Pool = require('pg').Pool
const conn = new Pool({
    user: 'whatsapp_sender',
    host: '188.166.184.95',
    database: 'whatsapp_sender',
    password: 'localhost',
    port: 5432,
})

// routes
app.get('/', function(req, res) {
    res.sendFile(__dirname + '/index.html');
});

app.get('/run', function(req, res, next) {
    req.app.io.of('/').emit('runEngine')
    res.send('oke');
});


/// socket connection
io.on('connection', function(socket) {
    app.io = io;
    console.log('a user connected');
    socket.on('updateStatus', function() {
        console.log('engine running:');
    });

    socket.on('dateChange', (data) => {
        return new Promise(function(resolve, reject) {
            let date = data[0]
            var sql = "SELECT CAST(created_at AS DATE) as grouped_date, count(*) FILTER (WHERE status = 'fail') AS fail, count(*) FILTER (WHERE status = 'sent') AS sukses, count(id) FROM messaging WHERE DATE(created_at) BETWEEN CAST('" + date.start + "' AS DATE) AND CAST('" + date.end + "' AS DATE) GROUP BY grouped_date ORDER BY grouped_date DESC";

            conn.query(sql, function(err, rows) {
                if (err) reject(err);

                let rowsa = rows.rows
                var total = [];
                var sent = [];
                var fail = [];
                var dataset = [];

                for (var d = new Date(date.start); d <= new Date(date.end); d.setDate(d.getDate() + 1)) {
                    dateFilter = new Date(d)
                    dataset.push(dateFormat(dateFilter, 'dd mmm'))
                    let dataSent = '';
                    let dataFail = '';
                    rowsa.forEach(element => {
                        if (dateFormat(element.grouped_date, 'ddmmyyHHMMss') == dateFormat(dateFilter, 'ddmmyyHHMMss')) {
                            dataSent = element.sukses
                            dataFail = element.fail
                        }
                    });
                    sent.push(parseInt(dataSent))
                    fail.push(parseInt(dataFail))
                }
                var stepInt = dataset.length
                var stepString = stepInt.toString()
                var totalSent = sent.filter(function(n) { return !isNaN(n); }).reduce((a, b) => a + b, 0)
                var totalFail = fail.filter(function(n) { return !isNaN(n); }).reduce((a, b) => a + b, 0)
                var totalMsg = totalSent + totalFail;
                // declare data untuk klien
                var response = {
                    sent: sent,
                    fail: fail,
                    dataset: dataset,
                    step: parseInt(stepInt > 10 ? stepString.slice(0, 1) : 1),
                    sentPercent: (totalSent / totalMsg) * 100,
                    failPercent: (totalFail / totalMsg) * 100
                };
                socket.emit('updateData', response)
                    // resolve(response);
            });
        })
    })
});



// functionss
function doStuff() {
    // let today = new Date()
    // let year = today.getFullYear()
    // let month = today.getMonth() < 10 ? "0" + today.getMonth() : today.getMonth()
    // let day = today.getDay() < 10 ? "0" + today.getDay() : today.getDay()
    // let hour = today.getHours() < 10 ? "0" + today.getHours() : today.getHours()
    // let minutes = today.getMinutes() < 10 ? "0" + today.getMinutes() : today.getMinutes()
    // let seconds = today.getSeconds() < 10 ? "0" + today.getSeconds() : today.getSeconds()
    getMessage()
        .then(res => {
            if (res) {
                updateOnProcess(res)
            } else {
                console.log('Belum ada Job')
            }
        })
        // console.log(year + '-' + month + '-' + day + ' ' + hour + ':' + minutes + ':' + seconds)
}

function checkStatus() {
    var request = require("request");

    var options = {
        method: 'POST',
        url: 'http://188.166.184.95:3000/status',
        headers: {
            'content-type': 'application/json'
        },
        json: true
    };

    request(options, function(error, response, body) {
        if (error) throw new Error(error);
        io.sockets.emit('updateStatus', body)
            // request.app.io.of('/').emit('updateStatus')
    });
}

setInterval(doStuff, 10000);
setInterval(checkStatus, 10000);
// doStuff()
// database conn
const getMessage = function() {
    return new Promise(function(resolve, reject) {
        conn.query("SELECT * FROM messaging WHERE status = 'on_queue' ORDER BY id ASC LIMIT 1", (error, results) => {
            if (error) reject(error);
            let data = results.rows
            resolve(data[0])
        })
    })
}

const updateOnProcess = function(data) {
    return new Promise(function(resolve, reject) {
        let id = data.id;
        let nomor = data.msisdn
        let pesan = data.pesan
        conn.query("UPDATE messaging SET status = 'on_process' WHERE id ='" + data.id + "'", (error, results) => {
            if (error) reject(error);
            resolve(results)
        })
    }).then(res => {
        let regex = /<br\s*[\/]?>/gi;
        let pesan = data.pesan
        var options = {
            method: 'POST',
            url: 'http://188.166.184.95:3000/',
            qs: {
                token: 'v0bvfzhiufx1430m'
            },
            headers: {
                'content-type': 'application/json'
            },
            body: {
                phone: data.msisdn,
                pesan: pesan.replace(regex, "\n")
            },
            json: true
        };

        request(options, function(error, response, body) {
            if (error) throw new Error(error);
            let status = body.sent == true ? 'sent' : 'fail'
            let keterangan = body.message
            conn.query("UPDATE messaging SET status = '" + status + ", keterangan = '" + keterangan + "' WHERE id ='" + data.id + "'", (error, results) => {
                if (error) reject(error);
                return (results)
            })
        });

    })

    const getData = function(data) {
        return new Promise(function(resolve, reject) {
            conn.query("SELECT * FROM messaging WHERE status = 'on_queue' ORDER BY id ASC LIMIT 1", (error, results) => {
                if (error) reject(error);
                let data = results.rows
                resolve(data[0])
            })
        })
    }
}