<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


    Route::get('/', 'HomeController@index');

// Auth::routes();
Auth::routes(['verify' => true]);

Route::group(['middleware' => ['auth', 'verified']], function() {
    Route::get('/home', 'HomeController@index');

    // modul phonebook
    Route::get('phonebook', 'PhonebookController@index');
    Route::get('/phonebook/create', 'PhonebookController@add');
    Route::post('/phonebook/create', 'PhonebookController@save');
    Route::post('/phonebook/saveExcel', 'PhonebookController@saveExcel');
    Route::post('/phonebook/edit', 'PhonebookController@update');
    Route::post('/phonebook/delete/{id}', 'PhonebookController@delete');
    Route::get('/phonebook/ajaxIndex', 'PhonebookController@ajaxIndex');
    Route::get('/phonebook/ajaxSelect', 'PhonebookController@ajaxSelect');
    Route::post('/phonebook/ajaxCekNomor', 'PhonebookController@ajaxCekNomor');

    Route::get('messaging/manual', 'MessagingController@manual');
    Route::post('messaging/manual', 'MessagingController@saveManual');
    Route::get('messaging/phonebook', 'MessagingController@phone');
    Route::post('messaging/phonebook', 'MessagingController@savephone');
    Route::get('messaging/xls', 'MessagingController@xls');
    Route::post('messaging/xls', 'MessagingController@savexls');
    Route::post('messaging/ajaxPesan', 'MessagingController@ajaxPesan');

    Route::get('log', 'LogController@index');
    Route::get('log/ajaxIndex', 'LogController@ajaxIndex');
    Route::get('queue', 'LogController@antrian');


});


// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
