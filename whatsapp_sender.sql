PGDMP         8        
        x            whatsapp_sender    9.5.19    9.6.8 v    �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false            �           1262    29794    whatsapp_sender    DATABASE     �   CREATE DATABASE whatsapp_sender WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_US.UTF-8' LC_CTYPE = 'en_US.UTF-8';
    DROP DATABASE whatsapp_sender;
             postgres    false                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
             postgres    false            �           0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                  postgres    false    6            �           0    0    SCHEMA public    ACL     �   REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;
                  postgres    false    6                        3079    12361    plpgsql 	   EXTENSION     ?   CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
    DROP EXTENSION plpgsql;
                  false            �           0    0    EXTENSION plpgsql    COMMENT     @   COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';
                       false    1            �            1259    29816    failed_jobs    TABLE     �   CREATE TABLE public.failed_jobs (
    id bigint NOT NULL,
    connection text NOT NULL,
    queue text NOT NULL,
    payload text NOT NULL,
    exception text NOT NULL,
    failed_at timestamp(0) without time zone DEFAULT now() NOT NULL
);
    DROP TABLE public.failed_jobs;
       public         postgres    false    6            �           0    0    TABLE failed_jobs    ACL     �   REVOKE ALL ON TABLE public.failed_jobs FROM PUBLIC;
REVOKE ALL ON TABLE public.failed_jobs FROM postgres;
GRANT ALL ON TABLE public.failed_jobs TO postgres;
GRANT SELECT,INSERT,UPDATE ON TABLE public.failed_jobs TO whatsapp_sender;
            public       postgres    false    190            �            1259    29798    failed_jobs_id_seq    SEQUENCE     {   CREATE SEQUENCE public.failed_jobs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.failed_jobs_id_seq;
       public       postgres    false    190    6            �           0    0    failed_jobs_id_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE public.failed_jobs_id_seq OWNED BY public.failed_jobs.id;
            public       postgres    false    181            �           0    0    SEQUENCE failed_jobs_id_seq    ACL     �   REVOKE ALL ON SEQUENCE public.failed_jobs_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE public.failed_jobs_id_seq FROM postgres;
GRANT ALL ON SEQUENCE public.failed_jobs_id_seq TO postgres;
GRANT ALL ON SEQUENCE public.failed_jobs_id_seq TO whatsapp_sender;
            public       postgres    false    181            �            1259    29824 	   messaging    TABLE     �  CREATE TABLE public.messaging (
    id bigint NOT NULL,
    id_perusahaan bigint NOT NULL,
    pesan text NOT NULL,
    status character varying(255) NOT NULL,
    req character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    keterangan character varying(255),
    msisdn character varying(255) NOT NULL,
    CONSTRAINT messaging_req_check CHECK (((req)::text = ANY (ARRAY[('api'::character varying)::text, ('app'::character varying)::text]))),
    CONSTRAINT messaging_status_check CHECK (((status)::text = ANY (ARRAY[('on_queue'::character varying)::text, ('on_process'::character varying)::text, ('sent'::character varying)::text, ('fail'::character varying)::text])))
);
    DROP TABLE public.messaging;
       public         postgres    false    6            �           0    0    TABLE messaging    ACL     �   REVOKE ALL ON TABLE public.messaging FROM PUBLIC;
REVOKE ALL ON TABLE public.messaging FROM postgres;
GRANT ALL ON TABLE public.messaging TO postgres;
GRANT SELECT,INSERT,UPDATE ON TABLE public.messaging TO whatsapp_sender;
            public       postgres    false    191            �            1259    29800    messaging_id_seq    SEQUENCE     y   CREATE SEQUENCE public.messaging_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.messaging_id_seq;
       public       postgres    false    6    191            �           0    0    messaging_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE public.messaging_id_seq OWNED BY public.messaging.id;
            public       postgres    false    182            �           0    0    SEQUENCE messaging_id_seq    ACL     �   REVOKE ALL ON SEQUENCE public.messaging_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE public.messaging_id_seq FROM postgres;
GRANT ALL ON SEQUENCE public.messaging_id_seq TO postgres;
GRANT ALL ON SEQUENCE public.messaging_id_seq TO whatsapp_sender;
            public       postgres    false    182            �            1259    29831 
   migrations    TABLE     �   CREATE TABLE public.migrations (
    id integer NOT NULL,
    migration character varying(255) NOT NULL,
    batch integer NOT NULL
);
    DROP TABLE public.migrations;
       public         postgres    false    6            �           0    0    TABLE migrations    ACL     �   REVOKE ALL ON TABLE public.migrations FROM PUBLIC;
REVOKE ALL ON TABLE public.migrations FROM postgres;
GRANT ALL ON TABLE public.migrations TO postgres;
GRANT SELECT,INSERT,UPDATE ON TABLE public.migrations TO whatsapp_sender;
            public       postgres    false    192            �            1259    29802    migrations_id_seq    SEQUENCE     z   CREATE SEQUENCE public.migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.migrations_id_seq;
       public       postgres    false    192    6            �           0    0    migrations_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.migrations_id_seq OWNED BY public.migrations.id;
            public       postgres    false    183            �           0    0    SEQUENCE migrations_id_seq    ACL     �   REVOKE ALL ON SEQUENCE public.migrations_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE public.migrations_id_seq FROM postgres;
GRANT ALL ON SEQUENCE public.migrations_id_seq TO postgres;
GRANT ALL ON SEQUENCE public.migrations_id_seq TO whatsapp_sender;
            public       postgres    false    183            �            1259    29835    model_has_permissions    TABLE     �   CREATE TABLE public.model_has_permissions (
    permission_id bigint NOT NULL,
    model_type character varying(255) NOT NULL,
    model_id bigint NOT NULL
);
 )   DROP TABLE public.model_has_permissions;
       public         postgres    false    6            �           0    0    TABLE model_has_permissions    ACL       REVOKE ALL ON TABLE public.model_has_permissions FROM PUBLIC;
REVOKE ALL ON TABLE public.model_has_permissions FROM postgres;
GRANT ALL ON TABLE public.model_has_permissions TO postgres;
GRANT SELECT,INSERT,UPDATE ON TABLE public.model_has_permissions TO whatsapp_sender;
            public       postgres    false    193            �            1259    29838    model_has_roles    TABLE     �   CREATE TABLE public.model_has_roles (
    role_id bigint NOT NULL,
    model_type character varying(255) NOT NULL,
    model_id bigint NOT NULL
);
 #   DROP TABLE public.model_has_roles;
       public         postgres    false    6            �           0    0    TABLE model_has_roles    ACL     �   REVOKE ALL ON TABLE public.model_has_roles FROM PUBLIC;
REVOKE ALL ON TABLE public.model_has_roles FROM postgres;
GRANT ALL ON TABLE public.model_has_roles TO postgres;
GRANT SELECT,INSERT,UPDATE ON TABLE public.model_has_roles TO whatsapp_sender;
            public       postgres    false    194            �            1259    29841    password_resets    TABLE     �   CREATE TABLE public.password_resets (
    email character varying(255) NOT NULL,
    token character varying(255) NOT NULL,
    created_at timestamp(0) without time zone
);
 #   DROP TABLE public.password_resets;
       public         postgres    false    6            �           0    0    TABLE password_resets    ACL     �   REVOKE ALL ON TABLE public.password_resets FROM PUBLIC;
REVOKE ALL ON TABLE public.password_resets FROM postgres;
GRANT ALL ON TABLE public.password_resets TO postgres;
GRANT SELECT,INSERT,UPDATE ON TABLE public.password_resets TO whatsapp_sender;
            public       postgres    false    195            �            1259    29847    permissions    TABLE     �   CREATE TABLE public.permissions (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    guard_name character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
    DROP TABLE public.permissions;
       public         postgres    false    6            �           0    0    TABLE permissions    ACL     �   REVOKE ALL ON TABLE public.permissions FROM PUBLIC;
REVOKE ALL ON TABLE public.permissions FROM postgres;
GRANT ALL ON TABLE public.permissions TO postgres;
GRANT SELECT,INSERT,UPDATE ON TABLE public.permissions TO whatsapp_sender;
            public       postgres    false    196            �            1259    29804    permissions_id_seq    SEQUENCE     {   CREATE SEQUENCE public.permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.permissions_id_seq;
       public       postgres    false    6    196            �           0    0    permissions_id_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE public.permissions_id_seq OWNED BY public.permissions.id;
            public       postgres    false    184            �           0    0    SEQUENCE permissions_id_seq    ACL     �   REVOKE ALL ON SEQUENCE public.permissions_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE public.permissions_id_seq FROM postgres;
GRANT ALL ON SEQUENCE public.permissions_id_seq TO postgres;
GRANT ALL ON SEQUENCE public.permissions_id_seq TO whatsapp_sender;
            public       postgres    false    184            �            1259    29854 
   perusahaan    TABLE     �  CREATE TABLE public.perusahaan (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    instance character varying(255),
    status character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone,
    CONSTRAINT perusahaan_status_check CHECK (((status)::text = ANY (ARRAY[('aktif'::character varying)::text, ('inaktif'::character varying)::text])))
);
    DROP TABLE public.perusahaan;
       public         postgres    false    6            �           0    0    TABLE perusahaan    ACL     �   REVOKE ALL ON TABLE public.perusahaan FROM PUBLIC;
REVOKE ALL ON TABLE public.perusahaan FROM postgres;
GRANT ALL ON TABLE public.perusahaan TO postgres;
GRANT SELECT,INSERT,UPDATE ON TABLE public.perusahaan TO whatsapp_sender;
            public       postgres    false    197            �            1259    29806    perusahaan_id_seq    SEQUENCE     z   CREATE SEQUENCE public.perusahaan_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.perusahaan_id_seq;
       public       postgres    false    197    6            �           0    0    perusahaan_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.perusahaan_id_seq OWNED BY public.perusahaan.id;
            public       postgres    false    185            �           0    0    SEQUENCE perusahaan_id_seq    ACL     �   REVOKE ALL ON SEQUENCE public.perusahaan_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE public.perusahaan_id_seq FROM postgres;
GRANT ALL ON SEQUENCE public.perusahaan_id_seq TO postgres;
GRANT ALL ON SEQUENCE public.perusahaan_id_seq TO whatsapp_sender;
            public       postgres    false    185            �            1259    29861 	   phonebook    TABLE     \  CREATE TABLE public.phonebook (
    id bigint NOT NULL,
    nomor character varying(255) NOT NULL,
    nama character varying(255),
    id_perusahaan bigint NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone,
    unique_id character varying(255)
);
    DROP TABLE public.phonebook;
       public         postgres    false    6            �           0    0    TABLE phonebook    ACL     �   REVOKE ALL ON TABLE public.phonebook FROM PUBLIC;
REVOKE ALL ON TABLE public.phonebook FROM postgres;
GRANT ALL ON TABLE public.phonebook TO postgres;
GRANT SELECT,INSERT,UPDATE ON TABLE public.phonebook TO whatsapp_sender;
            public       postgres    false    198            �            1259    29808    phonebook_id_seq    SEQUENCE     y   CREATE SEQUENCE public.phonebook_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.phonebook_id_seq;
       public       postgres    false    198    6            �           0    0    phonebook_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE public.phonebook_id_seq OWNED BY public.phonebook.id;
            public       postgres    false    186            �           0    0    SEQUENCE phonebook_id_seq    ACL     �   REVOKE ALL ON SEQUENCE public.phonebook_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE public.phonebook_id_seq FROM postgres;
GRANT ALL ON SEQUENCE public.phonebook_id_seq TO postgres;
GRANT ALL ON SEQUENCE public.phonebook_id_seq TO whatsapp_sender;
            public       postgres    false    186            �            1259    29868    role_has_permissions    TABLE     m   CREATE TABLE public.role_has_permissions (
    permission_id bigint NOT NULL,
    role_id bigint NOT NULL
);
 (   DROP TABLE public.role_has_permissions;
       public         postgres    false    6            �           0    0    TABLE role_has_permissions    ACL       REVOKE ALL ON TABLE public.role_has_permissions FROM PUBLIC;
REVOKE ALL ON TABLE public.role_has_permissions FROM postgres;
GRANT ALL ON TABLE public.role_has_permissions TO postgres;
GRANT SELECT,INSERT,UPDATE ON TABLE public.role_has_permissions TO whatsapp_sender;
            public       postgres    false    199            �            1259    29871    roles    TABLE     �   CREATE TABLE public.roles (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    guard_name character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
    DROP TABLE public.roles;
       public         postgres    false    6            �           0    0    TABLE roles    ACL     �   REVOKE ALL ON TABLE public.roles FROM PUBLIC;
REVOKE ALL ON TABLE public.roles FROM postgres;
GRANT ALL ON TABLE public.roles TO postgres;
GRANT SELECT,INSERT,UPDATE ON TABLE public.roles TO whatsapp_sender;
            public       postgres    false    200            �            1259    29810    roles_id_seq    SEQUENCE     u   CREATE SEQUENCE public.roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.roles_id_seq;
       public       postgres    false    200    6            �           0    0    roles_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE public.roles_id_seq OWNED BY public.roles.id;
            public       postgres    false    187            �           0    0    SEQUENCE roles_id_seq    ACL     �   REVOKE ALL ON SEQUENCE public.roles_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE public.roles_id_seq FROM postgres;
GRANT ALL ON SEQUENCE public.roles_id_seq TO postgres;
GRANT ALL ON SEQUENCE public.roles_id_seq TO whatsapp_sender;
            public       postgres    false    187            �            1259    29878    table_kd_phone_country    TABLE       CREATE TABLE public.table_kd_phone_country (
    id bigint NOT NULL,
    kode integer NOT NULL,
    negara character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);
 *   DROP TABLE public.table_kd_phone_country;
       public         postgres    false    6            �           0    0    TABLE table_kd_phone_country    ACL       REVOKE ALL ON TABLE public.table_kd_phone_country FROM PUBLIC;
REVOKE ALL ON TABLE public.table_kd_phone_country FROM postgres;
GRANT ALL ON TABLE public.table_kd_phone_country TO postgres;
GRANT SELECT,INSERT,UPDATE ON TABLE public.table_kd_phone_country TO whatsapp_sender;
            public       postgres    false    201            �            1259    29812    table_kd_phone_country_id_seq    SEQUENCE     �   CREATE SEQUENCE public.table_kd_phone_country_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 4   DROP SEQUENCE public.table_kd_phone_country_id_seq;
       public       postgres    false    201    6            �           0    0    table_kd_phone_country_id_seq    SEQUENCE OWNED BY     _   ALTER SEQUENCE public.table_kd_phone_country_id_seq OWNED BY public.table_kd_phone_country.id;
            public       postgres    false    188            �           0    0 &   SEQUENCE table_kd_phone_country_id_seq    ACL     +  REVOKE ALL ON SEQUENCE public.table_kd_phone_country_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE public.table_kd_phone_country_id_seq FROM postgres;
GRANT ALL ON SEQUENCE public.table_kd_phone_country_id_seq TO postgres;
GRANT ALL ON SEQUENCE public.table_kd_phone_country_id_seq TO whatsapp_sender;
            public       postgres    false    188            �            1259    29882    test    TABLE     7   CREATE TABLE public.test (
    "ID" bigint NOT NULL
);
    DROP TABLE public.test;
       public         postgres    false    6            �           0    0 
   TABLE test    ACL     �   REVOKE ALL ON TABLE public.test FROM PUBLIC;
REVOKE ALL ON TABLE public.test FROM postgres;
GRANT ALL ON TABLE public.test TO postgres;
GRANT SELECT,INSERT,UPDATE ON TABLE public.test TO whatsapp_sender;
            public       postgres    false    202            �            1259    29885    users    TABLE     �  CREATE TABLE public.users (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    email_verified_at timestamp(0) without time zone,
    password character varying(255) NOT NULL,
    remember_token character varying(100),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    id_perusahaan bigint,
    deleted_at timestamp(0) without time zone
);
    DROP TABLE public.users;
       public         postgres    false    6            �           0    0    TABLE users    ACL     �   REVOKE ALL ON TABLE public.users FROM PUBLIC;
REVOKE ALL ON TABLE public.users FROM postgres;
GRANT ALL ON TABLE public.users TO postgres;
GRANT SELECT,INSERT,UPDATE ON TABLE public.users TO whatsapp_sender;
            public       postgres    false    203            �            1259    29814    users_id_seq    SEQUENCE     u   CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.users_id_seq;
       public       postgres    false    203    6            �           0    0    users_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;
            public       postgres    false    189            �           0    0    SEQUENCE users_id_seq    ACL     �   REVOKE ALL ON SEQUENCE public.users_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE public.users_id_seq FROM postgres;
GRANT ALL ON SEQUENCE public.users_id_seq TO postgres;
GRANT ALL ON SEQUENCE public.users_id_seq TO whatsapp_sender;
            public       postgres    false    189                       2604    29819    failed_jobs id    DEFAULT     p   ALTER TABLE ONLY public.failed_jobs ALTER COLUMN id SET DEFAULT nextval('public.failed_jobs_id_seq'::regclass);
 =   ALTER TABLE public.failed_jobs ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    181    190    190                       2604    29827    messaging id    DEFAULT     l   ALTER TABLE ONLY public.messaging ALTER COLUMN id SET DEFAULT nextval('public.messaging_id_seq'::regclass);
 ;   ALTER TABLE public.messaging ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    191    182    191                       2604    29834    migrations id    DEFAULT     n   ALTER TABLE ONLY public.migrations ALTER COLUMN id SET DEFAULT nextval('public.migrations_id_seq'::regclass);
 <   ALTER TABLE public.migrations ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    183    192    192                       2604    29850    permissions id    DEFAULT     p   ALTER TABLE ONLY public.permissions ALTER COLUMN id SET DEFAULT nextval('public.permissions_id_seq'::regclass);
 =   ALTER TABLE public.permissions ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    184    196    196                       2604    29857    perusahaan id    DEFAULT     n   ALTER TABLE ONLY public.perusahaan ALTER COLUMN id SET DEFAULT nextval('public.perusahaan_id_seq'::regclass);
 <   ALTER TABLE public.perusahaan ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    185    197    197                       2604    29864    phonebook id    DEFAULT     l   ALTER TABLE ONLY public.phonebook ALTER COLUMN id SET DEFAULT nextval('public.phonebook_id_seq'::regclass);
 ;   ALTER TABLE public.phonebook ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    186    198    198                       2604    29874    roles id    DEFAULT     d   ALTER TABLE ONLY public.roles ALTER COLUMN id SET DEFAULT nextval('public.roles_id_seq'::regclass);
 7   ALTER TABLE public.roles ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    187    200    200                       2604    29881    table_kd_phone_country id    DEFAULT     �   ALTER TABLE ONLY public.table_kd_phone_country ALTER COLUMN id SET DEFAULT nextval('public.table_kd_phone_country_id_seq'::regclass);
 H   ALTER TABLE public.table_kd_phone_country ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    201    188    201                       2604    29888    users id    DEFAULT     d   ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);
 7   ALTER TABLE public.users ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    203    189    203            �          0    29816    failed_jobs 
   TABLE DATA               [   COPY public.failed_jobs (id, connection, queue, payload, exception, failed_at) FROM stdin;
    public       postgres    false    190   =�       �           0    0    failed_jobs_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.failed_jobs_id_seq', 2, false);
            public       postgres    false    181            �          0    29824 	   messaging 
   TABLE DATA               v   COPY public.messaging (id, id_perusahaan, pesan, status, req, created_at, updated_at, keterangan, msisdn) FROM stdin;
    public       postgres    false    191   Z�       �           0    0    messaging_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.messaging_id_seq', 12, true);
            public       postgres    false    182            �          0    29831 
   migrations 
   TABLE DATA               :   COPY public.migrations (id, migration, batch) FROM stdin;
    public       postgres    false    192   r�       �           0    0    migrations_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.migrations_id_seq', 15, true);
            public       postgres    false    183            �          0    29835    model_has_permissions 
   TABLE DATA               T   COPY public.model_has_permissions (permission_id, model_type, model_id) FROM stdin;
    public       postgres    false    193   ��       �          0    29838    model_has_roles 
   TABLE DATA               H   COPY public.model_has_roles (role_id, model_type, model_id) FROM stdin;
    public       postgres    false    194   ��       �          0    29841    password_resets 
   TABLE DATA               C   COPY public.password_resets (email, token, created_at) FROM stdin;
    public       postgres    false    195   ܔ       �          0    29847    permissions 
   TABLE DATA               S   COPY public.permissions (id, name, guard_name, created_at, updated_at) FROM stdin;
    public       postgres    false    196   [�       �           0    0    permissions_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.permissions_id_seq', 2, false);
            public       postgres    false    184            �          0    29854 
   perusahaan 
   TABLE DATA               d   COPY public.perusahaan (id, name, instance, status, created_at, updated_at, deleted_at) FROM stdin;
    public       postgres    false    197   x�       �           0    0    perusahaan_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.perusahaan_id_seq', 2, false);
            public       postgres    false    185            �          0    29861 	   phonebook 
   TABLE DATA               r   COPY public.phonebook (id, nomor, nama, id_perusahaan, created_at, updated_at, deleted_at, unique_id) FROM stdin;
    public       postgres    false    198   ��       �           0    0    phonebook_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.phonebook_id_seq', 73, true);
            public       postgres    false    186            �          0    29868    role_has_permissions 
   TABLE DATA               F   COPY public.role_has_permissions (permission_id, role_id) FROM stdin;
    public       postgres    false    199   Ӗ       �          0    29871    roles 
   TABLE DATA               M   COPY public.roles (id, name, guard_name, created_at, updated_at) FROM stdin;
    public       postgres    false    200   �       �           0    0    roles_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.roles_id_seq', 2, false);
            public       postgres    false    187            �          0    29878    table_kd_phone_country 
   TABLE DATA               f   COPY public.table_kd_phone_country (id, kode, negara, created_at, updated_at, deleted_at) FROM stdin;
    public       postgres    false    201   �       �           0    0    table_kd_phone_country_id_seq    SEQUENCE SET     M   SELECT pg_catalog.setval('public.table_kd_phone_country_id_seq', 233, true);
            public       postgres    false    188            �          0    29882    test 
   TABLE DATA               $   COPY public.test ("ID") FROM stdin;
    public       postgres    false    202   ��       �          0    29885    users 
   TABLE DATA               �   COPY public.users (id, name, email, email_verified_at, password, remember_token, created_at, updated_at, id_perusahaan, deleted_at) FROM stdin;
    public       postgres    false    203   ��       �           0    0    users_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('public.users_id_seq', 5, true);
            public       postgres    false    189                       2606    29893    failed_jobs failed_jobs_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.failed_jobs
    ADD CONSTRAINT failed_jobs_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY public.failed_jobs DROP CONSTRAINT failed_jobs_pkey;
       public         postgres    false    190    190                       2606    29897    messaging messaging_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.messaging
    ADD CONSTRAINT messaging_pkey PRIMARY KEY (id);
 B   ALTER TABLE ONLY public.messaging DROP CONSTRAINT messaging_pkey;
       public         postgres    false    191    191                        2606    29899    migrations migrations_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.migrations
    ADD CONSTRAINT migrations_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.migrations DROP CONSTRAINT migrations_pkey;
       public         postgres    false    192    192            #           2606    29902 0   model_has_permissions model_has_permissions_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.model_has_permissions
    ADD CONSTRAINT model_has_permissions_pkey PRIMARY KEY (permission_id, model_id, model_type);
 Z   ALTER TABLE ONLY public.model_has_permissions DROP CONSTRAINT model_has_permissions_pkey;
       public         postgres    false    193    193    193    193            &           2606    29905 $   model_has_roles model_has_roles_pkey 
   CONSTRAINT     }   ALTER TABLE ONLY public.model_has_roles
    ADD CONSTRAINT model_has_roles_pkey PRIMARY KEY (role_id, model_id, model_type);
 N   ALTER TABLE ONLY public.model_has_roles DROP CONSTRAINT model_has_roles_pkey;
       public         postgres    false    194    194    194    194            )           2606    29908    permissions permissions_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.permissions
    ADD CONSTRAINT permissions_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY public.permissions DROP CONSTRAINT permissions_pkey;
       public         postgres    false    196    196            +           2606    29911    perusahaan perusahaan_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.perusahaan
    ADD CONSTRAINT perusahaan_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.perusahaan DROP CONSTRAINT perusahaan_pkey;
       public         postgres    false    197    197            -           2606    29913    phonebook phonebook_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.phonebook
    ADD CONSTRAINT phonebook_pkey PRIMARY KEY (id);
 B   ALTER TABLE ONLY public.phonebook DROP CONSTRAINT phonebook_pkey;
       public         postgres    false    198    198            /           2606    29915 .   role_has_permissions role_has_permissions_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.role_has_permissions
    ADD CONSTRAINT role_has_permissions_pkey PRIMARY KEY (permission_id, role_id);
 X   ALTER TABLE ONLY public.role_has_permissions DROP CONSTRAINT role_has_permissions_pkey;
       public         postgres    false    199    199    199            1           2606    29917    roles roles_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.roles DROP CONSTRAINT roles_pkey;
       public         postgres    false    200    200            3           2606    29919 2   table_kd_phone_country table_kd_phone_country_pkey 
   CONSTRAINT     p   ALTER TABLE ONLY public.table_kd_phone_country
    ADD CONSTRAINT table_kd_phone_country_pkey PRIMARY KEY (id);
 \   ALTER TABLE ONLY public.table_kd_phone_country DROP CONSTRAINT table_kd_phone_country_pkey;
       public         postgres    false    201    201            5           2606    29921    test test_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.test
    ADD CONSTRAINT test_pkey PRIMARY KEY ("ID");
 8   ALTER TABLE ONLY public.test DROP CONSTRAINT test_pkey;
       public         postgres    false    202    202            7           2606    29923    users users_email_unique 
   CONSTRAINT     T   ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_email_unique UNIQUE (email);
 B   ALTER TABLE ONLY public.users DROP CONSTRAINT users_email_unique;
       public         postgres    false    203    203            9           2606    29925    users users_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.users DROP CONSTRAINT users_pkey;
       public         postgres    false    203    203            !           1259    29900 /   model_has_permissions_model_id_model_type_index    INDEX     �   CREATE INDEX model_has_permissions_model_id_model_type_index ON public.model_has_permissions USING btree (model_id, model_type);
 C   DROP INDEX public.model_has_permissions_model_id_model_type_index;
       public         postgres    false    193    193            $           1259    29903 )   model_has_roles_model_id_model_type_index    INDEX     u   CREATE INDEX model_has_roles_model_id_model_type_index ON public.model_has_roles USING btree (model_id, model_type);
 =   DROP INDEX public.model_has_roles_model_id_model_type_index;
       public         postgres    false    194    194            '           1259    29906    password_resets_email_index    INDEX     X   CREATE INDEX password_resets_email_index ON public.password_resets USING btree (email);
 /   DROP INDEX public.password_resets_email_index;
       public         postgres    false    195            :           2606    29926 A   model_has_permissions model_has_permissions_permission_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY public.model_has_permissions
    ADD CONSTRAINT model_has_permissions_permission_id_foreign FOREIGN KEY (permission_id) REFERENCES public.permissions(id) ON DELETE CASCADE;
 k   ALTER TABLE ONLY public.model_has_permissions DROP CONSTRAINT model_has_permissions_permission_id_foreign;
       public       postgres    false    2089    193    196            ;           2606    29931 /   model_has_roles model_has_roles_role_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY public.model_has_roles
    ADD CONSTRAINT model_has_roles_role_id_foreign FOREIGN KEY (role_id) REFERENCES public.roles(id) ON DELETE CASCADE;
 Y   ALTER TABLE ONLY public.model_has_roles DROP CONSTRAINT model_has_roles_role_id_foreign;
       public       postgres    false    200    194    2097            <           2606    29936 ?   role_has_permissions role_has_permissions_permission_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY public.role_has_permissions
    ADD CONSTRAINT role_has_permissions_permission_id_foreign FOREIGN KEY (permission_id) REFERENCES public.permissions(id) ON DELETE CASCADE;
 i   ALTER TABLE ONLY public.role_has_permissions DROP CONSTRAINT role_has_permissions_permission_id_foreign;
       public       postgres    false    2089    199    196            =           2606    29941 9   role_has_permissions role_has_permissions_role_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY public.role_has_permissions
    ADD CONSTRAINT role_has_permissions_role_id_foreign FOREIGN KEY (role_id) REFERENCES public.roles(id) ON DELETE CASCADE;
 c   ALTER TABLE ONLY public.role_has_permissions DROP CONSTRAINT role_has_permissions_role_id_foreign;
       public       postgres    false    200    199    2097            �           826    29951    DEFAULT PRIVILEGES FOR TABLES    DEFAULT ACL     �   ALTER DEFAULT PRIVILEGES FOR ROLE postgres REVOKE ALL ON TABLES  FROM PUBLIC;
ALTER DEFAULT PRIVILEGES FOR ROLE postgres REVOKE ALL ON TABLES  FROM postgres;
                  postgres    false            �      x������ � �      �     x���MO�0��ɯ� �Y�8�qA;p�%a��j����K��	���Ȗ�>N�Z3`!�I�Oy_+1�,�=C@�E!�xIaY�\â�����O%�m,!��XF!Q�s�ЃZ���H�\�1Vс�qG���u��r��z��Eml�6|�M�R���tH>�vhs���E�1��T���i���Gr˭�%��Cw���殲��R�ݖr��M���t%$R ��<�m$ܐd�l��/E-����H����۫�k����M      �      x�e��n� F���L�߻L�H�Z�:���#4�͆7���64��B�y98�+a�"�zZ�&�A8�7�EZ�ܔ���{P޴]�ໟ�ʸ��sy��o�k����zw�c�ۊ������>k��`ݑV��J�����K���0t��b�'�N;,����)U� �1h͡_��[����0�Q��{c8����ݴ�A�Y����Ф�b�D�S����]��*>�c]Z���ۻ�JA�Snv�K����Ա��5��EU�6�2��+4{}a�� 1 �      �      x������ � �      �      x������ � �      �   o   x�KLK��q�NLJ�L���L�T1�T14PqJ3*��.ʌ�vuNO���20J�N5sI��O�0��̩J�2�r�4��p�(��r*��4202�50�54V04�24�2������� �U      �      x������ � �      �      x�3��NLJ�L���CF\1z\\\ |�"      �      x���Kn�0D��)r�)ʎw�@N���Z���t���/Ə#�: ��Yz�>*ܷ���01]��|����.�dqEr���㱲�3r,y1��_\���I��ʖĺ�[�q`�c@o��&������?*90�=��3��E0M�뚀�0ϳ�L@�1,��1P���u��	��۶y���1����10i}p��˅�!�A�2�W�Thk}���g��@h}Wsh��.n���-;���l!؅z��;���l�ȡ��}��i�߃�b�jn9�o���%��      �      x������ � �      �      x������ � �      �   u	  x��X�n�:]����;��HI���g:�$�������1�f�`@I1⯟S��[Ecp�P|���!��R���;ӹ~0�B�uw��SB@���be�7�
����v;��}��4���f�Y�̞%�Z���?�7��h\��l�R�o�yU�7��B����>��ĥPY��7ss%�ғ}t�	!%���F�H�W�4��p�n�F(Qil"��beƭ�c�ȱ^�Y|߱������r�H8���øaf-4<1�C0��YzB��U��6l�{f���L�fo��G*U��=Ǣ/EY& �]c����W)�M�J[�ϡ��X�F�6Hq�N��1���M�E�Ⱦc�DR��4�;ڹ9�*�;~�<�h|h��R::ޏ<�S� |���V�nҲ �S���G��<�)�CӸ�?�du�!��5ss.4!��u�O��4���3r�nlv��=-pn�b5m����W�бc��V��R|4/v�h�v��,㣭�S��
;��7۹U�\����H�V�Th��'뭩�8�r��'۵&�s���S���m�8�%
!���o]�<��`*>�#�3́
��A�6��'�C)>7�HK"A���>b30$�������h%������:��q/�1ȉ4/�tT|&���2?�ќ����n�p�\\Yx���T\�yB�+c��f���^k�m�!����VS8���;���"�`[ØX��@W���5<�i��P�|A���S�MM9@+�E���W�X2	̙����#��& �;�D� ���A���F��lϿ�R�:�1H�q��v���?> ���`�̼5��U��u0�
)
Q�d��GQ"�)�	��L����Rb�}0v�N%<P��!jC%��Ė���P��!(~)����Rf�<̶Z��ej�۷9���\,�������� X��(!�&�ʲ4�h�}�q*=�����Rhd�Ҿ���ŭ	��4s� ��=h
�Ѹ'�AGU��ҡ,��@儹�aY�JF�/}�2�T�_���N!&;1�Y����L��w�;��*�����GT&�	j��-ъ�h�H�$��0~<���N�j�*��gu-n;�LHH���ha� "�q�L�����a��@7D{�qi'p����AJj�)��i?خ,SRRsH�c�u%tb
���޶�1�HA�[$���5c^IZ9�i�>�5Ǳ`��e���(�b<�EO�hJ	�Ih2\��\p,T�g�@B-*E����� �d��A�4�T�����ez [�.Z��Z�Ӡ�4�����w����雭��z�;-S�5�T��R�ER�����Іo���s� T⃖��O�1w�u�h)�CA�#��;�l��������Ę����N�"�C�݅��l~�F҇XÇ��*CB�Th���/!A�����BQ���#7����b����&ZD�߃�[��s�Wf�X�f�3	�1�Ɲ�X�O����oBA��ޢ�>9���S�Z��#Y.�*?�agDz#��nx��y���`��G˜�֘ʇa��УS���~�a�D�,/QϲB@�\�}�Z����K"����D=��Gr����ԢԤ��_�gɄU��5��4��'iM]$�@dowټ�>��iԃyd5UQqFW�Oy� k��~.�H]���5��.����� 	�iP�эIjz�H�l�k��-�D+l(�%��ﱸ�tQ�d��n�Ġ��)��>�mL�`��(�qx�L8�o!E>u���S@p�g�5�����R谷�mYi!.�א��y�C���9�K7'�ug_y!B�b7�y���=�$T�~tݓ�i��-�������Y���y�Ջi�);Ē��h�~������Ǡ'��r�@J�칾�F%݉$p4^�ЉiIH��)bP�ʾ=��Mt�=���jvc���IA� F^V�`/�mF�Ӧ�̪��{�>��7�`���z+��oZ�hO+\�<;1�*�ō�xWtE���m��_@�]�q9O� C�k�$鋏@Ka�J���V��٘���m
��e�L����:����OA�3��s��F�\p�ef�>�pm��E�T���l�6����ܨ��KrQe+��![w���þ7�;Gf�s���M���6V�OkϘ�\��7k_[�ԉ(4aM�ifO��1�����ߘhb�4����I�(Ɗ��̠m����5�%�b��zDt�>��L�����t1��{M����Q�����uĂ���|=+��{#qA¹"丱�W��T�GӍf`��U��#�O�7H��qm���+u&�:�BC�Q�Y��K�`������?L-�$�F��K��BO���6�Eɐ��WDS
5��k7fs���_�|���� &4�      �      x������ � �      �   �   x�3�tLK���L�ىI���yz�)�FF�F�F
fV�V�Ɯ*F�*�*�Ni�EY��.��FA�A����zUi��&&�Y��A9U��9�zUF�~�&.&�ɕ�1~p3�̭LM�-��c T�e��ę��tWJA�^z>�ehhet�!V1��b���� �"P     