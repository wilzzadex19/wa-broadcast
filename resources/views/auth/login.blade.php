
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico">

    <title>Login Whatsapp Broadcaster</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/sign-in/">

    <!-- Bootstrap core CSS -->
    <link href="{{rsc('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{rsc('css/signin.css')}}" rel="stylesheet">

  </head>

  <body class="text-center">
        <form method="POST" action="{{ route('login') }}" class="form-signin">
            @csrf
            <img src="{{ rsc('../logos/logo.png') }}" alt="" style="height:72px;">
            <h1 class="h3 mb-3 font-weight-normal">WA Broadcaster</h1>
            <label for="inputEmail" class="sr-only">Email address</label>
            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror mb-2" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="Email">
            @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
            <label for="inputPassword" class="sr-only">Password</label>
            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Password">
            <div class="checkbox mb-3">
        <!-- <label>
          <input type="checkbox" value="remember-me"> Remember me
        </label> -->
      </div>
      <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
      <p class="mt-5 mb-3 text-muted">Kabayan Consulting &copy; {{date('Y')}}</p>
    </form>
  </body>
</html>