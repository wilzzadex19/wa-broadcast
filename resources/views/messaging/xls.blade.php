@extends('layouts.admin')

@section('css')
    <link  href="{{rsc('vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">


@endsection

@section('content')
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">
        Kirim Pesan
        <span style="font-size:50%">
            <nav aria-label="breadcrumb" style="float:right;">
                <ol class="breadcrumb" style="margin-bottom: -1rem">
                    <li class="breadcrumb-item">Upload Excel</li>
                </ol>
            </nav>
        </span>
    </h1>
</div>

<!-- Content Row -->
<div class="row">
    <div class="col-md-12">
        <div class="card shadow mb-4">
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary">Form Upload Data Excel</h6>
            </div>
            <div class="card-body">
                <form action="{{ url('messaging/xls') }}" method="post" class="row" enctype="multipart/form-data">
                    @csrf
                    <div class="col-md-12">
                        @if ($message = Session::get('error'))
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            {{ $message }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @endif
                        @if ($message = Session::get('pesanKosong'))
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            {{ $message }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @endif
                        @if ($message = Session::get('sukses'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {!! $message !!}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @endif
                    </div>
                    <div class="col-md-2">
                        <a href="{{ url('public/template/phone_number_message_template.xlsx') }}" class="btn btn-info btn-block mb-4" title="download template excel"><i class="fa fa-download"></i>&nbsp;Download Template</a>
                    </div>
                    <div class="col-md-10">
                        <div class="custom-file mb-4">
                            <input type="file" name="file_excel" class="custom-file-input" id="customFile">
                            <label class="custom-file-label" for="customFile">Upload Excel nomor telepon</label>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="nomor_telepon">Isi Pesan</label>
                            <textarea name="pesan" id="" class="form-control" style="width: 100%" rows="10">@if ($message = Session::get('pesan')){{$message}}@endif</textarea>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <button class="btn btn-primary btn-block mt-4 btn-excel" disabled type="submit"><i class="fa fa-save"></i> Simpan </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script>
    $(document).on('change', '#customFile', function () {
        var fileName = $(this).val();
        //replace the "Choose a file" label
        $(this).next('.custom-file-label').html(fileName);
        if( document.getElementById("customFile").files.length == 0 ){
            $('.btn-excel').prop('disabled', true);
        } else {
            $('.btn-excel').prop('disabled', false);
        }
    })
</script>
@endsection
