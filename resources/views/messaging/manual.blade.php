@extends('layouts.admin')

@section('css')
    <link href="{{rsc('css/select2.min.css')}}" rel="stylesheet" />

@endsection

@section('content')
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">
        Kirim Pesan
        <span style="font-size:50%">
            <nav aria-label="breadcrumb" style="float:right; margin-left:10px;">
                <ol class="breadcrumb" style="margin-bottom: -1rem">
                    <li class="breadcrumb-item">Manual</li>
                </ol>
            </nav>
        </span>
    </h1>
</div>

<!-- Content Row -->
<div class="row">
    <div class="col-md-12">
        <div class="card shadow mb-4">
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary">Form Input Data</h6>
            </div>
            <div class="card-body">
                <form action="{{url('messaging/manual')}}" method="post" class="row">
                    @csrf
                    <div class="col-md-12">
                        @if ($message = Session::get('nomorKosong'))
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            {{ $message }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @endif

                        @if ($message = Session::get('pesanKosong'))
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            {{ $message }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @endif
                        @if ($message = Session::get('sukses'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {!! $message !!}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @endif
                    </div>
                    <div class="col-md-6">

                        <div class="form-group">
                            <label for="nomor_telepon">Nomor Whatsapp</label>
                            <select class="form-control" multiple name="nomor[]" id="nomor_telepon">
                            @if ($message = Session::get('nomor'))
                                @foreach($message as $key => $value)
                                    <option value="{{$value}}" selected>{{$value}}</option>
                                @endforeach
                            @endif
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nomor_telepon">Isi Pesan</label>
                            <textarea name="pesan" id="" class="form-control" style="width: 100%" rows="10">@if ($message = Session::get('pesan')){{$message}}@endif</textarea>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-primary btn-block">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


@endsection

@section('js')
<script src="{{rsc('js/select2.min.js')}}"></script>
<script>
    $("#nomor_telepon").select2({
        tags: true,
    });

</script>
@endsection
