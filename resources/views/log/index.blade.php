@extends('layouts.admin')

@section('css')
    <link  href="{{rsc('vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">


@endsection

@section('content')
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Log Pesan</h1>
    <!-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a> -->
</div>

<!-- Content Row -->
<div class="row">
    <div class="col-md-12">
        <div class="card shadow">
            <!-- Card Header - Dropdown -->
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary">Data Pengiriman Pesan</h6>
                <!-- <a href="{{ url('phonebook/create') }}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-plus fa-sm text-white-50"></i> Tambah Data</a> -->
            </div>
            <!-- Card Body -->
            <div class="card-body">
            @if ($message = Session::get('status'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{ $message }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endif

                <table id="table" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th width=130>Nomor Tujuan</th>
                            <th>Pesan</th>
                            <th>Status</th>
                            <th>Keterangan</th>
                            <th width=150>Tanggal Request</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection

@section('js')
    <script src="{{rsc('vendor/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{rsc('vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>
    <script>

        $(document).ready( function () {
            var table = $('#table').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                order: [[4, 'desc']],
                ajax: {
                    url : "{!! url('log/ajaxIndex') !!}",
                },
                columns: [
                    // { data: 'id', name: 'id' },
                    { data: 'msisdn', name: 'msisdn' },
                    { data: 'pesan', name: 'pesan' },
                    { data: 'status', name: 'status' },
                    { data: 'keterangan', name: 'keterangan' },
                    { data: 'dateTime', name: 'created_at' },

                ],
            });
            // fungsi search
            // table.columns().every( function () {
            //     var column = this;

            //     function searchThisColumn() {
            //         if(column.search() !== this.value) {
            //             column
            //                 .search(this.value)
            //                 .draw();
            //         }
            //     }

            //     $('#via', this.footer() ).on('change', searchThisColumn);
            //     $('#jenis', this.footer() ).on('change', searchThisColumn);
            // } );
        } );

        $(document).on('click','.edit', function () {
            var nama = $(this).data('nama');
            var nomor = $(this).data('nomor');
            var id = $(this).data('id');
            $('.helper').hide();
            $('#id').val(id);
            $('#nama').val(nama);
            $('#nomor').val(nomor);
        });

        $(document).on('click','.cek-data', function (e) {
                var nama = $('#nama').val();
                var nomor = $('#nomor').val();
                $.ajax({
                    url : "{{ url('phonebook/ajaxCekNomor') }}",
                    method : 'POST',
                    dataType : 'JSON',
                    data : {
                        'nomor' : nomor,
                        'nama' : nama,
                        "_token": "{{ csrf_token() }}",
                    },
                    beforeSend : function () {
                        $('.helper').hide();
                        $('.loading').toggle();
                    },
                    success : function (response) {
                        $('.loading').toggle();
                        if (response.isExist == true) {
                            $('#nomorHelp').text('Nomor diatas sudah terdaftar atas nama ' + (JSON.parse(response.data)).nama ).show();
                        } else {
                            $('#editModal').modal('toggle');
                            $('.form-edit').submit();
                        }
                    }
                })
        })

    </script>
@endsection
