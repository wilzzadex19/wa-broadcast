@extends('layouts.admin')

@section('css')
    <link  href="{{rsc('vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">


@endsection

@section('content')
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Antrian Pesan</h1>
    <!-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a> -->
</div>

<!-- Content Row -->
<div class="row">
    <div class="col-md-12">
        <div class="card shadow">
            <!-- Card Header - Dropdown -->
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary">Data Antrian Pesan</h6>
                <!-- <a href="{{ url('phonebook/create') }}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-plus fa-sm text-white-50"></i> Tambah Data</a> -->
            </div>
            <!-- Card Body -->
            <div class="card-body">
            @if ($message = Session::get('status'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{ $message }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endif

            @if($data->isEmpty())
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                Tidak ada pesan Dalam Antrian
            </div>
            @else
            <table id="table" class="table table-striped table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>Nomor Tujuan</th>
                        <th>Pesan</th>
                        <th>Tanggal Request</th>
                        <th>Keterangan</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($data as $key => $value)
                    <tr>
                        <td>{{ $value->msisdn }}</td>
                        <td><small>{!! $value->pesan !!}</small></td>
                        <td style="width: 200px;">{{ $value->created_at }}</td>
                        <td>{{ $value->keterangan }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            @endif
            </div>
        </div>
    </div>
</div>

@endsection

